# AlchemistDemo
This repository contains a demo of the Alchemist GIS module. 

1. Download the repository
2. Launch the jar AlchemistDemo
3. Load project Vienna whit button "Open"
4. Run simulation with button "RUN"
5. Press "P" on keyboard for start the simulation

The instruction of the Graphical interface is aviabile [here](https://alchemistsimulator.github.io/pages/tutorial/swingui/)
